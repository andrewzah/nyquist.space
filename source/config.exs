use Mix.Config

config :pleroma, Pleroma.Upload,
  uploads: "/uploads"

config :pleroma, Pleroma.Web.Endpoint,
  http: [port: 4000],
  protocol: "http",
  url: [host: "nyquist.space", scheme: "https", port: 443]

config :pleroma, :instance,
  name: "nyquist.space",
  email: "zah@andrewzah.com",
  limit: 650,
  registrations_open: true,
  federating: true

config :pleroma, :media_proxy,
  enabled: true,
  redirect_on_failure: true,
  base_url: "https://media.nyquist.space"

config :pleroma, Pleroma.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: "db",
  username: "pleroma",
  database: "pleroma_prod",
  pool_size: 10

config :pleroma, :chat, enabled: false

config :logger,
  level: :info

config :tzdata, :data_dir, "/tmp/elixir_tzdata_data"

import_config "prod.secret.exs"


