import os

p = os.getcwd()
f = open("emoji.txt", "w")

files = []
for (dirpath, dirnames, filenames) in os.walk(p):
	files.extend(filenames)

for file in files:
	base, ext = os.path.splitext(file)
	if ext == ".png":
		f.write("{base}, /emoji/{file}\n".format(base=base, file=file))

f.close()
